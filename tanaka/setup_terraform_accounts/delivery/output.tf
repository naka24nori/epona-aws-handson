output "terraformer_arn" {
  description = "Terraform実行用ユーザのARN"
  value       = module.delivery.terraformer_users
}

output "terraform_execution_role_arn" {
  description = "Terraform実行ロールのARN"
  value       = module.delivery.terraform_execution_role_arn
}
